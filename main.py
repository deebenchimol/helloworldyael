import micro_webapp2
import webapp2
from jinja2 import Environment, FileSystemLoader
import get_campaign_list

app = micro_webapp2.WSGIApplication()
env = Environment(loader=FileSystemLoader('templates'))

@app.route('/')
def hello_handler(request, *args, **kwargs):
    campaigns = get_campaign_list.return_campaign_list()['entries']    
    template = env.get_template('index.html')
    return template.render(campaigns=campaigns)
    

def main():
    app.run()

if __name__ == '__main__':
    main()